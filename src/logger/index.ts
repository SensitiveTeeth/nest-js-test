import buildDevLogger from '@/logger/dev-logger';
import buildProdLogger from '@/logger/prod-logger';

function getLogger() {
  if (process.env.NODE_ENV === 'production') {
    return buildProdLogger();
  } else {
    return buildDevLogger();
  }
}
const logger = getLogger();
export default logger;
