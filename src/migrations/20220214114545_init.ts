import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable('user');
  if (hasTable) return;
  return knex.schema.createTable('user', (table) => {
    table.increments();
    table.string('name');
    table.string('gender');
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTableIfExists('user');
}
