export const envVariable = declareEnvVariable();
export function declareEnvVariable() {
  if (process.env.NODE_ENV === 'production') {
    return {};
  }

  // default set as development
  return {};
}
